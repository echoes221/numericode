'use strict';

import { createStore, applyMiddleware } from 'redux';
import rootReducer from 'ducks';
import { subscribe } from 'utils/local-storage';
import thunk from 'redux-thunk';
import api from 'utils/api';

// Create redux store and pass middlewares
const store = createStore(rootReducer, applyMiddleware(thunk.withExtraArgument(api)));

// Subscribe store to local storage
subscribe(store);

export default store;