'use strict';

import { combineReducers } from 'redux';
import numericode from './numericode';

export default combineReducers({
    numericode
});