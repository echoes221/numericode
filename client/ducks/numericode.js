'use strict';

import { lastStored } from 'utils/local-storage';
import { get } from 'lodash';

// Action Namespace
const ns = 'NUMERICODE_';
const stateName = 'numericode';

// Action Types
const FETCHING = `${ns}FETCHING`;
const RESULT = `${ns}RESULT`;
const ENCRYPTED = `${ns}ENCRYPTED`;

// Initial State
const initialState = get(lastStored, stateName, {
    fetching: false,
    result: '',
    encrypted: ''
});

// In case window refreshes during fetch
// Should default to false on reload
initialState.fetching = false;

// Reducer
export default function reducer (state = initialState, action) {
    switch (action.type) {
        case FETCHING:
            return {
                ...state,
                fetching: action.fetching
            };
        case RESULT:
            return {
                ...state,
                result: action.result
            };
        case ENCRYPTED:
            return {
                ...state,
                encrypted: action.encrypted
            }
        default:
            return state;
    }
}

// Action Creators
export const updateFetchState = (fetching) => ({ type: FETCHING, fetching });
export const updateResult = (result) => ({ type: RESULT, result });
export const updateEncrypted = (encrypted) => ({ type: ENCRYPTED, encrypted });

/**
 * Thunk action. Calls decrypt api
 * @param {String} encrypted 
 * @return {Function}
 */
export const decrypt = (encrypted) => {
    return (dispatch, getState, api) => {
        dispatch(updateFetchState(true));

        return api.decrypt(encrypted).then(({ result }) => {
            dispatch(updateFetchState(false));
            dispatch(updateResult(result));
        })
        .catch(err => dispatch(updateFetchState(false)));
    }
};