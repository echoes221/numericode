'use strict';

import React from 'react';
import store from '../store';
import { Provider } from 'react-redux';
import Numericode from './numericode';

/**
 * Render Application structure
 * Pass any providers & dependencies
 * 
 * @function App
 * @return {Component} App
 */
const App = () => {
    return (
        <Provider store = {store} >
            <Numericode />
        </Provider>
    );
};

export default App;