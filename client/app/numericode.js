'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'ducks/numericode';
import TextBox from 'components/text-box';
import Button from 'components/button';
import { parseInt, trim } from 'lodash';

/**
 * Renders out and controls numericode decryption
 * input, decryption and result displaying
 * 
 * @class Numericode
 */
class Numericode extends Component {
    constructor (props) {
        super(props);

        this.onTextBoxChange = this.onTextBoxChange.bind(this);
        this.onButtonPress = this.onButtonPress.bind(this);
    }

    /**
     * Handles changes to the text box
     * Updates props.encrypted value
     * 
     * @for Numericode
     * @method onTextBoxChange
     * @param {Object} event 
     */
    onTextBoxChange (event) {
        const { updateEncrypted } = this.props;

        updateEncrypted(event.target.value);
    }

    /**
     * Handles button press
     * Calls a fetch thunk
     * 
     * @for Numericode
     * @method onButtonPress
     */
    onButtonPress () {
        const { encrypted, decrypt } = this.props;
        
        if (this.validateEncrypted(encrypted)) {
            decrypt(trim(encrypted)); 
        } else {
            alert('Input must be all numbers and spaces, no other characters allowed!');
        }
    }

    /**
     * Attempts to loosely validate a string to see if all values are numbers
     * Returns truthy if so
     * 
     * @for Numericode
     * @method validateEncrypted
     * @param {String} encrypted 
     * @return {Boolean}
     */
    validateEncrypted (encrypted) {
        return encrypted.split(' ').every(val => !isNaN(val));
    }

    render() {
        const { encrypted, result, fetching } = this.props;

        return (
            <div style = {{ padding: '10px' }}>
                <TextBox
                    value = {encrypted}
                    onChange = {this.onTextBoxChange}
                    placeholder = {'Numbers To Be Decrypted'}
                />
                <Button 
                    onClick = {this.onButtonPress} 
                    label = {'Decrypt!'}
                    disabled = {fetching}
                />
                {result ? <div>{`Result: ${result}`}</div> : null }
            </div>
        );
    }
}

Numericode.propTypes = {
    // Props
    fetching: PropTypes.bool.isRequired,
    result: PropTypes.string.isRequired,
    encrypted: PropTypes.string.isRequired,

    // Actions
    updateEncrypted: PropTypes.func.isRequired,
    decrypt: PropTypes.func.isRequired
};

const mapStateToProps = ({numericode}) => ({
    ...numericode
});

const mapDispatchToProps = (dispatch) => ({
   ...bindActionCreators(actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Numericode);