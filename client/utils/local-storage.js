'use strict';

const storageKey = 'NUMERICODE';

/**
 * Gets the last stored store at runtime
 *
 * @var lastStored
 * @return {Object} store
 */
export const lastStored = (() => {
    let data = window.localStorage.getItem(storageKey);

    if (!data) {
        return {};
    }

    try {
        data = JSON.parse(decodeURIComponent(data));
    } catch (e) {
        return {};
    }

    return data;
})();

/**
 * Subscribe to changes in store and update localstorage with new state
 *
 * @function subscribe
 * @param {Object} store
 */
export const subscribe = (store) => {
    store.subscribe(() => window.localStorage.setItem(
        storageKey, 
        encodeURIComponent(JSON.stringify(store.getState()))
    ));
};