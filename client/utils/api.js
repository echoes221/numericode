'use strict';

import 'whatwg-fetch';

/**
 * Makes a post request to the server to decrypt
 * an encrypted string
 * 
 * @function decrypt
 * @param {String} encrypted 
 * @return {Promise}
 */
const decrypt = (encrypted) => {
    return new Promise((resolve, reject) => {
        window.fetch('/decrypt', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ encrypted })
        })
        .then(response => response.json())
        .then(body => resolve(body))
        .catch(err => reject(err));
    });
};

export default {
    decrypt
};