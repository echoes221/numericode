'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const style = {
    display: 'block',
    width: '100px',
    height: '20px'
};

/**
 * Renders out a simple button component
 * 
 * @function Button
 * @param {Function} onClick
 * @param {String} label
 * @param {Boolean} disabled
 * @return {Component} button
 */
const Button = ({ onClick, label, disabled }) => (
    <button onClick = {onClick} disabled = {disabled} style = {style}>
        {label}
    </button>
);

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string,
    disabled: PropTypes.bool
};

export default Button;