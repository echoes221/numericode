'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const style = {
    width: '500px',
    height: '250px'
};

/**
 * Renders out a simple text box
 * 
 * @function TextBox
 * @param {Function} onChange
 * @param {String} value
 * @param {String} placeholder
 * @return {Component} TextBox
 */
const TextBox = ({ onChange, value, placeholder}) => (
    <textarea
        onChange = {onChange}
        value = {value}
        placeholder = {placeholder}
        style = {style}
    >
    </textarea>
);

TextBox.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string
};

export default TextBox;