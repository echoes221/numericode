'use strict';

const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const requestLogger = require('./middlewares/req-logger');
const handlers = require('./handlers');

const router = () => {
    const app = express();
    const server = http.createServer(app);

    /**
     * Listen on passed port
     * 
     * @method listen
     * @param {Integer} port
     * @return {Promise}
     */
    const listen = (port) => Promise.resolve(server.listen(port));

    /**
     * Closes the server
     * 
     * @method close
     */
    const close = () => server.close();

    /**
     * Returns the server address
     * 
     * @method address
     */
    const address = () => server.address();

    // Register middlewares
    app.use(bodyParser.json());
    app.use(requestLogger);

    // Register statics
    app.use(express.static(path.join(__dirname, '../view')));

    // Register Routes
    app.post('/decrypt', handlers.decrypt);

    return {
        listen,
        close,
        address
    };
};

module.exports = router;