'use strict';

const numericode = require('../utils/numericode');

/**
 * Decrypts an encrypted string of numbers
 * 
 * @function decrypt
 * @param {String} req.query.encrypted
 */
const decrypt = (req, res) => {
    const encrypted = req.body.encrypted.split(' ').map(string => parseInt(string, 10));

    // If values aren't all numbers, throw away request
    if (!encrypted.every(val => !isNaN(val))) {
        return res.status(400).send('Not A Valid Request');
    }
    
    res.json({
       result: numericode.decryptToString(encrypted)
    });
};

module.exports = decrypt;