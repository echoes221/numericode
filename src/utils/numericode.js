'use strict';

const ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
const LIMIT = 27;
const SPACE = ' ';

/**
 * Check if a number is over the limit
 * 
 * @function overLimit
 * @param {Number} num
 * @return {Boolean}
 */
const overLimit = (num) => num >= LIMIT;

/**
 * Brings a number under the limit set by
 * dividing by limit. 
 * Recursive
 * 
 * @function getUnderLimit
 * @param {Number} num
 * @return {Number}
 */
const getUnderLimit = (num) => overLimit(num) ? getUnderLimit(num / LIMIT) : num;

/**
 * Decrypts all numbers in an array
 * If number is an integer will find it's place in the alphabet
 * array (after finding if its under the limit)
 * if its not an integer will return a space
 * 
 * @function decrypt
 * @param {Array} numArr array of numbers
 * @return {Array}
 */
const decrypt = (numArr) => numArr.map((number) => {
    number = getUnderLimit(number);

    return Number.isInteger(number) ? ALPHABET[--number] : SPACE;
});

/**
 * Same as decrypt except it returns the result in string format
 * 
 * @function decryptToString
 * @param numArr array of numbers
 * @return {String}
 */
const decryptToString = (numArr) => decrypt(numArr).join('');

module.exports = {
    overLimit,
    getUnderLimit,
    decrypt,
    decryptToString
};