### 0 Android, iPhone or Other? Why?
I'm an Android man myself. I'm a tinkerer and always sideloading/flashing latest version of android and playing about with various roms. I prefer a more stock experience though so usually purchase in the nexus lineup of devices. I find android a lot more customisable for my needs, insofar as being able to set default apps and have a sane notification draw. I understand the appeal of iPhones but I find them too locked down unless they're jailbroken, but that destroys your warranty so why bother. Oh and the price of iPhones makes me cringe and itunes can jump in a fire.

### 1 What is your working environment like now? what do you want it to be?
It used to be entertaining and engaging, currently it's a little depressing and there's not much difference between me working from home or in the office. I wan't my working enviroment to be a little more social and collaborative like how things used to be around this place and be a conductive environment that pushes people to learn and try new things

### ++1 (1++ would still be 1 until the next iteration!) A mysterious relative gives you so much money that you never have to work a day for the rest of your life. What do I do?
I'd buy several chalets in various ski resorts around the world and spend my time snowboarding & travelling. Though I'd probably still spend my summers coding as I enjoy the challenge. A man can dream.

### 3 (I don't think I can triforce in MD) What's the worst bug I've ever made?
I've made plenty of bugs over my years of coding, I can't think of one that sticks out as the 'worst' but the most memorable was changing some logic in an untested legacy system that was a "crush one and five more pop up" kind of situation that killed some of our log events, that took some time to get right and many attempts. I usually try to catch those at QA stage though. Testing and Linting help considerably to iron those out.

### 2^2 Describe your coding style in 3 words or less.
Function over form

### S Have I done pair programming before?
Yes on some occasions, usually when getting the specs down or wondering into territory that I'm unfamilliar with and my collegue is. I find it a much quicker way of learning than trawling through the docs and the partner can explain caveats and gotchas before they arise. The other way round I enjoy teaching in these situations. Qaulity output can be higher too. Dislikes can be lack of keyboard time and sometimes you just want to get on with a feature/project yourself so speed can be an issue.

### 110 How is the code you write now different from the code you wrote 2 years ago
A lot cleaner, split out (more on the functional side of things) and efficient. I'm a lot more confident in my ability now than back then too, though being self taught, imposter syndrome can raise it's head from time to time.

### t What one thing would you most like to change about the software engineering process at your current workplace
Would love to have more visibility on the devops side of things. My remit currently stops outside of a jenkins deployment and being able to scale on marathon. I'd love to delve deeper end to end.

### 2^3 Due to a clerical error, we accidentally hire you as CEO. What's the first thing you do?
Ask HR to fix the issue and find someone who is more suited to be CEO! (But first maybe do something fun for the company... who knows.)

### 3^2 If you were writing this questionnaire, what would you ask?
"Which would you rather fight, 1 horse sized duck? Or a 100 duck sized horses?"

For the record, I'd fight the horse sized duck, then ride it into glorious battle against the 100 duck sized horses. It would be legendary.






