'use strict';

import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { expect } from 'chai';
import Button from 'components/button';

describe('<Button />', () => {
    it('should render', () => {
        const wrapper = shallow(<Button label = 'foo' onClick = {() => {}} />);
        
        expect(wrapper.exists()).to.equal(true);
    });

    it('should click', () => {
        const spy = sinon.spy();
        const wrapper = shallow(<Button label = 'foo' onClick = {spy} />);

        wrapper.find('button').simulate('click');
        expect(spy.calledOnce).to.equal(true);
    });
});