'use strict';

import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { expect } from 'chai';
import TextBox from 'components/text-box';

describe('<TextBox />', () => {
    it('should render', () => {
        const wrapper = shallow(<TextBox value = 'foo' onChange = {() => {}} />);
        
        expect(wrapper.exists()).to.equal(true);
    });

    it('should propagate changes', () => {
        const spy = sinon.spy();
        const wrapper = shallow(<TextBox value = 'foo' onChange = {spy} />);

        wrapper.find('textarea').simulate('change', { target: { value: 'foobar' } });
        expect(spy.calledOnce).to.equal(true);
    });
});