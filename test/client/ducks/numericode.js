'use strict';

import { expect } from 'chai';
import sinon from 'sinon';
import reducer from 'ducks/numericode';
import * as actions from 'ducks/numericode';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

describe('Numericode Reducer', () => {
    describe('updateFetchState', () => {
        it('should set the fetch state to true', () => {
            const state = reducer({}, actions.updateFetchState(true));

            expect(state.fetching).to.equal(true);
        });

        it('should set the fetch state to false', () => {
            const state = reducer({}, actions.updateFetchState(false));

            expect(state.fetching).to.equal(false);
        });
    });

    describe('updateResult', () => {
        it('should update the result value', () => {
            const state = reducer({}, actions.updateResult('FooBar'));

            expect(state.result).to.equal('FooBar');
        });
    });

    describe('updateEncrypted', () => {
        it('should update the encrypted value', () => {
            const state = reducer({}, actions.updateEncrypted('1234 5678'));

            expect(state.encrypted).to.equal('1234 5678');
        });
    });

    describe('decrypt', () => {
        it('should call the API endpoint and dispatch relevant actions on success', (done) => {
            const api = {
                decrypt: sinon.stub().returns(Promise.resolve({ result: 'FooBar' }))
            };
            const mockStore = configureStore([thunk.withExtraArgument(api)]);
            const store = mockStore({});

            const expectedActions = [
                { type: 'NUMERICODE_FETCHING', fetching: true },
                { type: 'NUMERICODE_FETCHING', fetching: false },
                { type: 'NUMERICODE_RESULT', result: 'FooBar' }
            ];

            store.dispatch(actions.decrypt('12345')).then(() => {
                expect(api.decrypt.calledOnce).to.equal(true);
                expect(store.getActions()).to.deep.equal(expectedActions);
                done();
            });
        });

        it('should call the API endpoint and dispatch the relevant actions on failure', (done) => {
            const api = {
                decrypt: sinon.stub().returns(Promise.reject({ error: 'Something Bad Happened!!' }))
            };
            const mockStore = configureStore([thunk.withExtraArgument(api)]);
            const store = mockStore({});

            const expectedActions = [
                { type: 'NUMERICODE_FETCHING', fetching: true },
                { type: 'NUMERICODE_FETCHING', fetching: false },
            ];

            store.dispatch(actions.decrypt('12345')).then(() => {
                expect(api.decrypt.calledOnce).to.equal(true);
                expect(store.getActions()).to.deep.equal(expectedActions);
                done();
            }); 
        });
    });
});