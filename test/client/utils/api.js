'use strict';

import { expect } from 'chai';
import sinon from 'sinon';
import api from 'utils/api';

beforeEach(() => {
    sinon.stub(window, 'fetch');
});

afterEach(() => {
    window.fetch.restore();
});

describe('API', () => {
    describe('decrypt', () => {
        describe('On successful request', () => {
            beforeEach(() => {
                const res = new window.Response('{"result": "FooBar"}', {
                    status: 200,
                    headers: {
                        'Content-type': 'application/json'
                    }
                });

                window.fetch.returns(Promise.resolve(res));
            });

            it('should fetch the payload from the query', (done) => {
                api.decrypt('1234').then((result) => {
                    expect(result).to.deep.equal({
                        result: 'FooBar'
                    });

                    done();
                });
            });
        });

        describe('On failed request', () => {
            beforeEach(() => {
                const res = new window.Response('{"error": "something went wrong"}', {
                    status: 400,
                    headers: {
                        'Content-type': 'application/json'
                    }
                });

                window.fetch.returns(Promise.reject(res));
            });

            it('should resolve to an error on bad request', (done) => {
                api.decrypt('%%!!').catch((err) => {
                    expect(err.status).to.equal(400);
                    done();
                });
            });
        });
    });
});