'use strict';

import { expect } from 'chai';
import { lastStored, subscribe } from 'utils/local-storage';

describe('local-storage', () => {
    describe('subscribe', () => {
        it('should store values on store changes in local storage', () => {
            // JSDOM has issues with local storage
        });
    });

    describe('lastStored', () => {
        it('should return an object', () => {
            expect(lastStored).to.be.an('object');
        });
    });
});