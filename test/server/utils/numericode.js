'use strict';

const chai = require('chai');
const expect = chai.expect;
const numericode = require('../../../src/utils/numericode');

const testCases = [
    [13, 27, 26, 5],
    [432, 21, 19, 5832, 5, 135, 14, 6561, 59049, 15, 486, 275562],
    [20, 486, 21, 513, 19, 324, 5, 21924, 540, 135, 3, 8],
    [8, 5, 324, 8748, 295245, 730, 23, 405, 13122, 12, 108]
];
const testResults = [
    'MAZE',
    'PUSHEENICORN',
    'TRUSSLE TECH',
    'HELLO WORLD'
];

describe('numericode', () => {
    describe('overLimit', () => {
        it('should return true if the value is equal to 27', () => {
            expect(numericode.overLimit(27)).to.equal(true);
        });

        it('should return true if a number is greater than 27', () => {
            expect(numericode.overLimit(53)).to.equal(true);
        });

        it('should return false if a number is under 27', () => {
            expect(numericode.overLimit(26)).to.equal(false);
        });
    });

    describe('getUnderLimit', () => {
        it('should return the number passed if number is under 27', () => {
            expect(numericode.getUnderLimit(10)).to.equal(10);
        });

        it('should divide the number by 27 if the number is equal to 27', () => {
            expect(numericode.getUnderLimit(27)).to.equal(1);
        });

        it('should divide the number by 27 if the number is greater than 27', () => {
            expect(numericode.getUnderLimit(216)).to.equal(8);
        });

        it('should bring a number under 27 by recursively calling itself if number is larger than 27 after division', () => {
            expect(numericode.getUnderLimit(3645)).to.equal(3645 / 27 / 27);
            // Typically should spy this, but recursive calls from without so can't spy from outside
        });
    });

    describe('decrypt', () => {
        it('should take an Integer that is lower than 27 and assign it a letter of alphabet', () => {
            expect(numericode.decrypt([1])).to.deep.equal(['A']);
        });

        it('should take a number that is not an integer and assign it a space " "', () => {
            expect(numericode.decrypt([5.4])).to.deep.equal([' ']);
        });

        it('should take an Integer that is greater than 27 and divide by 27 until under limit and assign an alphabetical value', () => {
            expect(numericode.decrypt([216])).to.deep.equal(['H']);
        });

        testCases.forEach((test, index) => {
            it(`should decrypt ${test} to ${testResults[index].split('')}`, () => {
                expect(numericode.decrypt(test)).to.deep.equal(testResults[index].split(''));
            });
        });
    });

    describe('decryptToString', () => {
        it('should take an Integer that is lower than 27 and assign it a letter of alphabet', () => {
            expect(numericode.decryptToString([1])).to.equal('A');
        });

        it('should take a number that is not an integer and assign it a space " "', () => {
            expect(numericode.decryptToString([5.4])).to.equal(' ');
        });

        it('should take an Integer that is greater than 27 and divide by 27 until under limit and assign an alphabetical value', () => {
            expect(numericode.decryptToString([216])).to.equal('H');
        });

        testCases.forEach((test, index) => {
            it(`should decrypt to string ${test} to ${testResults[index]}`, () => {
                expect(numericode.decryptToString(test)).to.equal(testResults[index]);
            });
        });
    });
});