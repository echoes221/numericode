'use strict';

const chai = require('chai');
const expect = chai.expect;

module.exports = () => {
    describe('on successful request', function () {
        it('should decrypt the payload and return the result', function (done) {
            this.agent.post('/decrypt')
                .send({
                    encrypted: '13 27 26 5'
                })
                .expect(200)
                .expect({
                    result: 'MAZE'
                })
            .end(done);
        });
    });

    describe('on unsuccessful request', function () {
        it('should return a 400 if the request is malformed', function (done) {
            this.agent.post('/decrypt')
                .send({
                    encrypted: '13 %%!@. 26 5'
                })
                .expect(400)
                .expect('Not A Valid Request')
            .end(done);
        });
    });
};