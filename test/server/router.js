'use strict';

const chai = require('chai');
const expect = chai.expect;
const supertest = require('supertest');
const router = require('../../src/router');

// Using standard func declarations to allow pasing of agent to all handlers
describe('router', () => {
    beforeEach(function () {
        this.router = router();

        this.agent = supertest.agent(this.router);
    });

    afterEach(function () {
        this.router.close();
    });

    describe('/decrypt', require('./handlers/decrypt'));
});