# Numericode
A substitution cipher is an algorithm that maps a letter onto something else - a letter, number or symbol. For example, you may have seen a numeric substitution cipher, where A = 1, B = 2, ..., Y = 25, Z = 26 . In this example, HELLO would become 85121215.
Numericode is very similar to the numeric substitution cipher, except that the numbers given can be larger than 26 (the number of letters in the alphabet). If a number is 27 or larger, it needs to be divided by 27 until it is 26 or less. To give an example, 8 5 12 12 15 is still a valid encoding of HELLO , but so is 216 3645 12 324 405 (because it is 8*27 5*27*27 12 12*27 15*27 ).

## Dev Notes
Server built on top of Node & Express.
Client built on top of React & Redux.

Whilst it's a little more complex than needed for a simple app, redux lends itself nicely to reloading it's state from an external store which allows easy warming of the store.
API calls are handled by a fetch polyfill plugging into an async action.

Further Work
 - More Tests!! I feel I've tested all the important pieces of the application/logic however, on both server and client side
 - Nicer CSS, I've left it very barebones
 - Better error handling! I'm currently not doing anything with failed requests to the server
 - I've implemented some rudimentary form input validation, but it could be better (e.g. updating on user input instead of upon submit)
 - Build an encryptor! Do the reverse on the server, including a random/jitter on multiplying by 27
 - Request caching, if we see the same request come it, we shouldn't need to run through the decryption logic again
 - I'm aware that recursion may slow things down decrypting depending on the value passed to it - something to look into for future


## Building

Obligatory For All
```
$ npm install
```
### Prod
To get running for production:
```
$ npm run build && npm run start
```
Navigate to http://localhost:1740

### Dev
Requires two console tabs. In first tab:
```
$ npm run start
 or if server building and you have nodemon installed
$ nodemon
```
In second tab:
```
$ npm run Dev
```
Navigate to `localhost:8080`. Webpack will rebuild changes live via hot reload

### Testing
```
$ npm run test
```