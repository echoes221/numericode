'use strict';

const config = require('./config');
const logger = require('winston');
const router = require('./src/router')();

process.on('uncaughtException', (err) => {
    logger.error(err);
    router.close();
});

process.on('unhandledRejection', (reason, promise) => {
    logger.error(`Unhandled Rejection ${reason} at ${promise}`);
    router.close();
});

router.listen(config.port).then(() => {
    logger.info(`Numericode server listening on ${config.port}`);
});